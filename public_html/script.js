$(document).ready(function(){
        
   $(".slideShow:first-child").addClass("current");
    
    
     $("span").each(
        function(i){
           $(this).addClass("unfold"+i); 
    });
    
    $(".content").each(
        function(i){
            $(this).addClass("unfold"+i); 
    });
   
    $("div.title span").click(function(){
        $("div."+$(this).attr("class")).toggle(200);
    }).toggle(function(){
        $(this).text("Hide");
    },
    function(){
        $(this).text("More");
    });
    
    changePic();
    
});

    function changePic(){
        
        var current = $(".current");
        current.animate({"opacity":"0"},1500,function(){
            $(this).removeClass("current");
            current = current.next(); 
            if(current[0] === undefined)
                current = $(".slideShow:first-child");
            current.css("opacity", "0").addClass("current").animate({"opacity":"1"},1500,changePic);
        });
    }     
    